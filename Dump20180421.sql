-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: news_bd
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--
CREATE SCHEMA `news_bd`;
use `news_bd`;
DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `new_id` int(10) unsigned NOT NULL,
  `author` varchar(255) DEFAULT 'Anonymous',
  `comment` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_new_id_idx` (`new_id`),
  CONSTRAINT `fk_new_id` FOREIGN KEY (`new_id`) REFERENCES `news` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (39,6,'alex','test'),(40,12,'fdsfs','fsdf'),(41,12,'fdsfs','fsdfds'),(44,10,'Anonymous','fdsfds'),(46,10,'Anonymous','fdsfdsfsd'),(50,6,'egor','lbmlsfdm;b');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image` text,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (6,'Новость','Страница просмотра одного поста. Здесь также выводится список всех комментариев к данному посту. Есть возможность удалить комментарии и добавить новый комментарий. При отправке комментария указывать ID новости, к которой создается данный комментарий.\r\n','Edp7doU3a3n8h53XBu_Le.png','2018-04-21 08:54:27'),(8,'Help','Список всех новостных постов с возможностью \r\nПерейти на страницу добавления поста (кнопка Add new post)\r\nПрочитать один новостной пост (ссылка Read Full Post >> )\r\nУдалить новостной пост (ссылка Delete)\r\n','fbO4pTIqVhDm0Sf2cqLbX.jpg','2018-04-21 09:21:32'),(10,'Новая новость','Для новости:\r\nGET /news - получить список новостей. При этом возвращается массив объектов, содержащих все поля, кроме \"Содержимое новости\"\r\nPOST /news - создать новость. При этом проверяются все поля на обязательность/необязательность и загружается файл изображения.\r\nGET /news/:id - получить одну новость по ID. При этом возвращается один объект со всеми полями.\r\nDELETE /news/:id - удалить одну новость по ID. Если у новости есть комментарии, их тоже нужно удалить.\r\n\r\nДля комментариев:\r\nGET /comments - получить список всех комментариев. Если передать ?news_id=ID, и указать в качестве ID идентификатор какой-то новости, то нужно получить список комментариев ТОЛЬКО к указанной новости.\r\nPOST /comments - создать комментарий. Проверить, что ID новости присутствует в запросе и такая новость существует.\r\nDELETE /comments/:id - удалить один комментарий по ID.\r\n','N_umr7uCk8TyfvnAJtF8t.jpg','2018-04-21 12:13:23'),(12,'Test','Контрольная работа 10\r\n\r\nНеобходимо создать приложение - новостной сайт с возможностью комментировать новости. Приложение состоит из двух частей - backend и frontend.\r\nОсновные сущности:\r\nНовость\r\nID новости (генерируется автоматически)\r\nЗаголовок новости (обязательное поле)\r\nСодержимое новости (обязательное поле)\r\nИзображение (необязательное поле)\r\nДата публикации (генерируется автоматически)\r\n\r\nКомментарий\r\nID комментария (генерируется автоматически)\r\nID новости (обязательное поле, нужно чтобы такая новость существовала)\r\nАвтор (необязательное поле, если не указан, вместо него пишется \"Anonymous\")\r\nКомментарий (обязательное поле)\r\n\r\nСерверная часть (backend)\r\nПредставляет из себя API для базовых CRUD-операций с вышеперечисленными сущностями:\r\n\r\nДля новости:\r\nGET /news - получить список новостей. При этом возвращается массив объектов, содержащих все поля, кроме \"Содержимое новости\"\r\nPOST /news - создать новость. При этом проверяются все поля на обязательность/необязательность и загружается файл изображения.\r\nGET /news/:id - получить одну новость по ID. При этом возвращается один объект со всеми полями.\r\nDELETE /news/:id - удалить одну новость по ID. Если у новости есть комментарии, их тоже нужно удалить.\r\n\r\nДля комментариев:\r\nGET /comments - получить список всех комментариев. Если передать ?news_id=ID, и указать в качестве ID идентификатор какой-то новости, то нужно получить список комментариев ТОЛЬКО к указанной новости.\r\nPOST /comments - создать комментарий. Проверить, что ID новости присутствует в запросе и такая новость существует.\r\nDELETE /comments/:id - удалить один комментарий по ID.\r\nКлиентская часть (frontend)\r\n\r\nСписок всех новостных постов с возможностью \r\nПерейти на страницу добавления поста (кнопка Add new post)\r\nПрочитать один новостной пост (ссылка Read Full Post >> )\r\nУдалить новостной пост (ссылка Delete)\r\n\r\n\r\n\r\n\r\nСтраница просмотра одного поста. Здесь также выводится список всех комментариев к данному посту. Есть возможность удалить комментарии и добавить новый комментарий. При отправке комментария указывать ID новости, к которой создается данный комментарий.\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nУказания\r\nХранение данных делаем любым способом (на выбор):\r\nВ файлах\r\nВ MySQL\r\nСоответственно, если вы делаете проверки целостности данных в MySQL, то можно не делать их вручную. \r\nПри сдаче проекта, необходимо предоставить:\r\nЕсли храните данные в файлах, то: пример файла с базой данных т.е. типа db.template.json\r\nЕсли храните данные в MySQL, то: файл создания базы данных (экспорт из MySQL или скрипт, написанный вручную). Проверьте, что он работает, я буду при проверке запускать его!\r\nПроект можно сдать в одном репозитории или в двух.\r\nКаждый репозиторий должен иметь как минимум 5 коммитов, если это один общий репозиторий - то как минимум 10 коммитов.\r\nBackend пишем на express.\r\nFrontend пишем на React + Redux.\r\nДизайн произвольный, используйте bootstrap или как захотите верстайте вручную.\r\n','N3yyeYU5RVu7ZRjdTVd60.png','2018-04-21 12:22:03'),(13,'Какая то новость','Основные сущности:\r\nНовость\r\nID новости (генерируется автоматически)\r\nЗаголовок новости (обязательное поле)\r\nСодержимое новости (обязательное поле)\r\nИзображение (необязательное поле)\r\nДата публикации (генерируется автоматически)\r\n\r\nКомментарий\r\nID комментария (генерируется автоматически)\r\nID новости (обязательное поле, нужно чтобы такая новость существовала)\r\nАвтор (необязательное поле, если не указан, вместо него пишется \"Anonymous\")\r\nКомментарий (обязательное поле)\r\n\r\nСерверная часть (backend)\r\n','pb2bqmr8DTqx84XiA9uUC.jpg','2018-04-21 12:33:45');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-21 19:16:41
