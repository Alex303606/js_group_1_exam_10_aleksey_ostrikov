const express = require('express');

const router = express.Router();

const createRouter = (db) => {
	router.get('/', (req, res) => {
		db.query('SELECT * FROM comments', function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results.map(comment => {
					return {
						id: comment.id,
						news_id: comment.new_id,
						author: comment.author,
						comment: comment.comment
					}
				}));
			}
		});
	});
	
	router.get('/news_id=:id', (req, res) => {
		db.query(`SELECT * FROM comments WHERE new_id=${req.params.id}`, function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results);
			}
		});
	});
	
	router.post('/', (req, res) => {
		const comment = req.body;
		if(!comment.author) comment.author = 'Anonymous';
		if (comment.new_id && comment.comment) {
			db.query(
				'INSERT INTO comments (new_id, author, comment) VALUES (?, ?, ?)',
				[comment.new_id, comment.author, comment.comment],
				(error, results) => {
					if (error) {
						res.status(500);
						res.send(error);
					} else {
						comment.id = results.insertId;
						res.send(comment);
					}
				}
			);
		} else {
			res.status(500);
			res.send('Заполните обязательные поля!');
		}
	});
	
	router.delete('/:id', (req, res) => {
		db.query(`DELETE FROM comments WHERE ID=${req.params.id}`, function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results);
			}
		});
	});
	
	return router;
};

module.exports = createRouter;