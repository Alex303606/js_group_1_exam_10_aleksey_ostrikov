const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

const createRouter = (db) => {
	router.get('/', (req, res) => {
		db.query('SELECT * FROM news', function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results.map(oneNews => {
					return {
						id: oneNews.id,
						title: oneNews.title,
						image: oneNews.image,
						date: oneNews.date
					}
				}));
			}
		});
	});
	
	router.get('/:id', (req, res) => {
		db.query(`SELECT * FROM news WHERE ID=${req.params.id}`, function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				if (results.length === 0) {
					res.status(404);
					res.send({msg: 'Not found'});
				} else {
					res.send(results);
				}
			}
		});
	});
	
	router.post('/', upload.single('image'), (req, res) => {
		const oneNews = req.body;
		if (req.file) {
			oneNews.image = req.file.filename;
		} else {
			oneNews.image = null;
		}
		
		if (oneNews.title && oneNews.content) {
			db.query(
				'INSERT INTO news (title,content, image) VALUES (?, ?, ?)',
				[oneNews.title, oneNews.content, oneNews.image],
				(error, results) => {
					if (error) {
						res.status(500);
						res.send(error);
					} else {
						oneNews.id = results.insertId;
						res.send(oneNews);
					}
				}
			);
		} else {
			res.status(500);
			res.send('Заполните обязательные поля!');
		}
	});
	
	router.delete('/:id', (req, res) => {
		db.query(`DELETE FROM news WHERE ID=${req.params.id}`, function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results);
			}
		});
	});
	
	return router;
};

module.exports = createRouter;