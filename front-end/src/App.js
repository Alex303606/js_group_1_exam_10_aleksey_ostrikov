import React, {Component, Fragment} from 'react';
import Header from "./components/Header/Header";
import News from "./containers/News/News";
import Add from "./containers/Add/Add";
import {Route, Switch} from "react-router-dom";
import OneNews from "./containers/OneNews/OneNews";
import PageNotFound from "./containers/PageNotFound/PageNotFound";

class App extends Component {
	render() {
		return (
			<Fragment>
				<Header/>
				<Switch>
					<Route exact path="/" component={News}/>
					<Route exact path="/news/:id" component={OneNews}/>
					<Route exact path="/add" component={Add}/>
					<Route component={PageNotFound}/>
				</Switch>
			</Fragment>
		);
	}
}

export default App;
