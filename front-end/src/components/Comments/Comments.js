import React, {Component} from 'react';
import './Comments.css';
import {connect} from "react-redux";
import {deleteComment, sendComment} from "../../store/actions";

class Comments extends Component {
	
	state = {
		new_id: this.props.id,
		author: '',
		comment: ''
	};
	
	inputChangeHandler = event => {
		event.persist();
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	sendCommentToServer = event => {
		event.preventDefault();
		this.props.sendComment(this.state).then(() => {
			this.setState({
				author: '',
				comment: ''
			});
		});
	};
	
	render() {
		return (
			<div className="comments">
				{this.props.comments.length === 0 && <h4>К этой публикации нет комментариев.</h4>}
				<h3>Комментарии к новости:</h3>
				<ul>
					{
						this.props.comments.map(comment => {
							return <li key={comment.id}>
								<div>
									<span>Написал <b>{comment.author}</b></span>
									<button onClick={() => this.props.deleteComment(comment.id)}>Удалить</button>
								</div>
								<p>{comment.comment}</p>
							</li>
						})
					}
				</ul>
				<h3>Добавить комментарий</h3>
				<form onSubmit={this.sendCommentToServer}>
					<div className="row">
						<label htmlFor="author">Автор:</label>
						<input value={this.state.author}
						       onChange={this.inputChangeHandler}
						       id="author"
						       name="author"
						       type="text"/>
					</div>
					<div className="row">
						<label htmlFor="comment">Комментарий:</label>
						<textarea placeholder={this.props.error.length !== 0 ? this.props.error : null}
						          style={this.props.error ? {borderColor: 'red', borderWidth: '2px'} : null}
						          value={this.state.comment}
						          onChange={this.inputChangeHandler}
						          id="comment"
						          name="comment"/>
					</div>
					<button>Отправить</button>
				</form>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		error: state.error
	};
};

const mapDispatchToProps = dispatch => {
	return {
		sendComment: (comment) => dispatch(sendComment(comment)),
		deleteComment: (id) => dispatch(deleteComment(id))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Comments);