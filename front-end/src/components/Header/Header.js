import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';

const Header = () => {
	return (
		<header>
			<NavLink exact to="/">Список новостей</NavLink>
			<NavLink exact to="/add">Добавить новость</NavLink>
		</header>
	)
};

export default Header;