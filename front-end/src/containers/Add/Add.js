import React, {Component} from 'react';
import './Add.css';
import {connect} from "react-redux";
import {postFormToServerDb} from "../../store/actions";

class Add extends Component {
	
	state = {
		title: '',
		content: '',
		image: null,
		preview: null
	};
	
	sendForm = event => {
		event.preventDefault();
		const formData = new FormData();
		Object.keys(this.state).forEach(key => {
			if (key !== 'preview') formData.append(key, this.state[key]);
		});
		this.props.sendFormToServer(formData).then(response => {
			if(!response.error) {
				this.setState({
					title: '',
					content: '',
					image: null,
					preview: null
				});
				this.props.history.push({pathname: '/'});
			}
		});
	};
	
	inputChangeHandler = event => {
		event.persist();
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	fileChangeHandler = event => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			
			reader.onload = (event) => {
				this.setState({preview: event.target.result});
			};
			
			reader.readAsDataURL(event.target.files[0]);
		}
		this.setState({
			[event.target.name]: event.target.files[0]
		});
	};
	
	
	render() {
		return (
			<div className="add-edit-item">
				<h2>Описание предмета:</h2>
				<form onSubmit={this.sendForm}>
					<div className="row">
						<label htmlFor="title">Заголовок *</label>
						<input placeholder={this.props.error ? this.props.error : null} style={this.props.error ? {borderColor: 'red',borderWidth: '2px'} : null} value={this.state.title} onChange={this.inputChangeHandler} id="title" name="title" type="text"/>
					</div>
					<div className="row">
						<label htmlFor="content">Текст *</label>
						<textarea
							placeholder={this.props.error ? this.props.error : null}
							style={this.props.error ? {borderColor: 'red',borderWidth: '2px'} : null} value={this.state.content}
							onChange={this.inputChangeHandler}
							id="content"
							name="content"/>
					</div>
					<div className="row">
						<label className="addImage" htmlFor="image">Добавить картинку</label>
						<input id="image"
						       style={{display: 'none'}}
						       name="image"
						       onChange={this.fileChangeHandler}
						       type="file"/>
						{this.state.preview && <img className="previewImg" src={this.state.preview} alt="previewImg"/>}
					</div>
					<button>Отправить</button>
				</form>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		error: state.error
	};
};

const mapDispatchToProps = dispatch => {
	return {
		sendFormToServer: (data) => dispatch(postFormToServerDb(data))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Add);
