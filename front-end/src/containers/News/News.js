import React, {Component, Fragment} from 'react';
import './News.css';
import {connect} from "react-redux";
import {deleteItem, getItems} from "../../store/actions";

class News extends Component {
	
	componentDidMount() {
		this.props.getAllItems();
	};
	
	getItem = id => {
		this.props.history.push({pathname: '/news/' + id})
	};
	
	deleteItem = id => {
		this.props.deleteItemFromDB(id);
	};
	
	render() {
		return (
			<Fragment>
				<div className="items">
					<ul>
						{
							this.props.news.map(oneNews => {
								return <li
									key={oneNews.id}
									className="this-item">
										{oneNews.image && <img src={'http://localhost:8000/uploads/' + oneNews.image} alt=""/>}
										<div className="info">
											<div className="left">
												<b onClick={() => this.getItem(oneNews.id)}>{oneNews.title}</b>
												<span className="date">Дата публикации: {oneNews.date}</span>
											</div>
											<button className="delete" onClick={() => this.deleteItem(oneNews.id)}>Удалить</button>
											<button onClick={() => this.getItem(oneNews.id)} className="edit">Подробнее...</button>
										</div>
									</li>
							})
						}
					</ul>
				</div>
			</Fragment>
		)
	}
}

const mapStateToProps = state => {
	return {
		news: state.news,
		error: state.error
	};
};

const mapDispatchToProps = dispatch => {
	return {
		getAllItems: () => dispatch(getItems()),
		deleteItemFromDB: (id) => dispatch(deleteItem(id))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(News);