import React,{Component} from 'react';
import './OneNews.css';
import {connect} from "react-redux";
import {getAllComments, getItemByID} from "../../store/actions";
import Comments from "../../components/Comments/Comments";

class OneNews extends Component {
	
	componentDidMount(){
		this.props.getItemByID(this.props.match.params.id).then(() => {
			if (this.props.error.msg) this.props.history.push({pathname: '/404'});
		});
		this.props.getAllComments(this.props.match.params.id);
	}
	
	render(){
		return (
			<div className="news">
				<h2>Заголовок: <span>{this.props.currentNews.title}</span></h2>
				<div className="date">Дата публикации: <span>{this.props.currentNews.date}</span></div>
				<p className="date">{this.props.currentNews.content}</p>
				{this.props.currentNews.image && <img src={'http://localhost:8000/uploads/' + this.props.currentNews.image} alt=""/>}
				<Comments id={this.props.match.params.id} error={this.props.error} comments={this.props.currentComments}/>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		items: state.items,
		error: state.error,
		currentNews: state.currentNews,
		currentComments: state.currentComments
	};
};

const mapDispatchToProps = dispatch => {
	return {
		getItemByID: (id) => dispatch(getItemByID(id)),
		getAllComments: (id) => dispatch(getAllComments(id))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(OneNews);