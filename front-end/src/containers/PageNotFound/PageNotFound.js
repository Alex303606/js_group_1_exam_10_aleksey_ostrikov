import React from 'react';

const PageNotFound = () => {
	return (
		<h1 style={{textAlign: 'center',fontSize: 70, paddingTop: '10%'}}>Страница ненайдена!!!</h1>
	)
};

export default PageNotFound;