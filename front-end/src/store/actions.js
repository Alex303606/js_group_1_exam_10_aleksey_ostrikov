import {
	FETCH_GET_SUCCESS,
	FETCH_POST_COMMENT_SUCCESS,
	FETCH_DELETE_COMMENT_SUCCESS,
	FETCH_GET_BY_ID_SUCCESS,
	FETCH_GET_COMMENTS_SUCCESS,
	FETCH_POST_SUCCESS,
	FETCH_POST_ERROR,
	FETCH_DELETE_SUCCESS
} from "./actionsTypes";

import axios from '../axios-api';

export const fetchPostSuccess = data => {
	return {type: FETCH_POST_SUCCESS, data};
};

export const fetchPostCommentSuccess = comment => {
	return {type: FETCH_POST_COMMENT_SUCCESS, comment};
};

export const fetchGetSuccess = news => {
	return {type: FETCH_GET_SUCCESS, news};
};

export const fetchGetByIDSuccess = currentNews => {
	return {type: FETCH_GET_BY_ID_SUCCESS, currentNews};
};

export const fetchGetCommentsSuccess = currentComments => {
	return {type: FETCH_GET_COMMENTS_SUCCESS, currentComments};
};

export const fetchPostError = error => {
	return {type: FETCH_POST_ERROR, error}
};

export const fetchDeleteSuccess = id => {
	return {type: FETCH_DELETE_SUCCESS, id}
};

export const fetchDeleteCommentSuccess = id => {
	return {type: FETCH_DELETE_COMMENT_SUCCESS, id}
};

export const postFormToServerDb = data => {
	return dispatch => {
		return axios.post('/news', data).then(
			response => dispatch(fetchPostSuccess(response.data))
		).catch(error => dispatch(fetchPostError(error)));
	}
};

export const getItems = () => {
	return dispatch => {
		return axios.get('/news').then(
			response => dispatch(fetchGetSuccess(response.data))
		).catch(error => dispatch(fetchPostError(error)));
	};
};

export const getItemByID = id => {
	return dispatch => {
		return axios.get(`/news/${id}`).then(
			response => dispatch(fetchGetByIDSuccess(response.data[0]))
		).catch(error => dispatch(fetchPostError(error)));
	}
};

export const deleteItem = id => {
	return dispatch => {
		return axios.delete(`/news/${id}`).then(
			() => dispatch(fetchDeleteSuccess(id))
		).catch(error => dispatch(fetchPostError(error)))
	}
};

export const getAllComments = id => {
	return dispatch => {
		return axios.get(`/comments/news_id=${id}`).then(
			response => dispatch(fetchGetCommentsSuccess(response.data))
		).catch(error => dispatch(fetchPostError(error)));
	}
};

export const sendComment = comment => {
	return dispatch => {
		return axios.post('/comments', comment).then(
			response => dispatch(fetchPostCommentSuccess(response.data))
		).catch(error => dispatch(fetchPostError(error)));
	}
};

export const deleteComment = id => {
	return dispatch => {
		return axios.delete(`/comments/${id}`).then(
			() => dispatch(fetchDeleteCommentSuccess(id))
		);
	}
};
