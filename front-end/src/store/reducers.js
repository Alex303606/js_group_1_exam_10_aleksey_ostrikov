import {
	FETCH_GET_SUCCESS,
	FETCH_POST_COMMENT_SUCCESS,
	FETCH_DELETE_COMMENT_SUCCESS,
	FETCH_GET_COMMENTS_SUCCESS,
	FETCH_GET_BY_ID_SUCCESS,
	FETCH_POST_SUCCESS,
	FETCH_POST_ERROR,
	FETCH_DELETE_SUCCESS
} from "./actionsTypes";

const initialState = {
	news: [],
	error: '',
	currentComments: [],
	currentNews: {}
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_POST_SUCCESS:
			const news = [...state.news];
			news.push(action.data);
			return {...state, news, error: ''};
		case FETCH_POST_COMMENT_SUCCESS:
			const currentComments = [...state.currentComments];
			currentComments.push(action.comment);
			return {...state, currentComments: currentComments, error: ''};
		case FETCH_POST_ERROR:
			return {...state, error: action.error.response.data};
		case FETCH_GET_SUCCESS:
			return {...state, news: action.news, error: ''};
		case FETCH_GET_BY_ID_SUCCESS:
			return {...state, currentNews: action.currentNews};
		case FETCH_GET_COMMENTS_SUCCESS:
			return {...state, currentComments: action.currentComments};
		case FETCH_DELETE_SUCCESS:
			const NewNews = [...state.news];
			const index = NewNews.findIndex(p => p.id === action.id);
			NewNews.splice(index, 1);
			return {...state, news: NewNews, error: ''};
		case FETCH_DELETE_COMMENT_SUCCESS:
			const comments = [...state.currentComments];
			const index2 = comments.findIndex(p => p.id === action.id);
			comments.splice(index2, 1);
			return {...state, currentComments: comments, error: ''};
		default:
			return state;
	}
};

export default reducer;
